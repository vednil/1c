# 1С в Docker

[[_TOC_]]


Для сборки образов используется платформа 1С версии 8.3.19-1150.

1. Загружаем с сайта https://releases.1c.ru архивы _deb64_8_3_19_1150.tar.gz_ и _client_8_3_19_1150.deb64.tar.gz_ в текущую директорию.
2. Клонируем репозиторий:
```
git clone git@gitlab.com:vednil/1c.git
```


## Сервер 1С

1. Извлекаем файлы для установки сервера 1С:
```
tar -xzf deb64_8_3_19_1150.tar.gz 1c-enterprise-8.3.19.1150-common_8.3.19-1150_amd64.deb
tar -xzf deb64_8_3_19_1150.tar.gz 1c-enterprise-8.3.19.1150-server_8.3.19-1150_amd64.deb
mv 1c-enterprise-8.3.19.1150-common_8.3.19-1150_amd64.deb 1c/1c-server/8.3.19-1150/
mv 1c-enterprise-8.3.19.1150-server_8.3.19-1150_amd64.deb 1c/1c-server/8.3.19-1150/
```
2. Собираем образ:
```
docker build -t 1c-server:8.3.19-1150 ./1c/1c-server/
```
3. Запускаем сервер 1С:
```
docker run -d \
           --privileged \
           -h 1c-server.example.com \
           -p 1540-1541:1540-1541 \
           -p 1560-1591:1560-1591 \
           -p 475:475/udp \
           -p 1947:1947 \
           -v 1c-server:/home/usr1cv8 \
        1c-server:8.3.19-1150
```


## Веб-сервер

1. Извлекаем файлы для установки веб-сервера:
```
tar -xzf deb64_8_3_19_1150.tar.gz 1c-enterprise-8.3.19.1150-common_8.3.19-1150_amd64.deb
tar -xzf deb64_8_3_19_1150.tar.gz 1c-enterprise-8.3.19.1150-server_8.3.19-1150_amd64.deb
tar -xzf deb64_8_3_19_1150.tar.gz 1c-enterprise-8.3.19.1150-ws_8.3.19-1150_amd64.deb
mv 1c-enterprise-8.3.19.1150-common_8.3.19-1150_amd64.deb 1c/1c-ws/8.3.19-1150/
mv 1c-enterprise-8.3.19.1150-server_8.3.19-1150_amd64.deb 1c/1c-ws/8.3.19-1150/
mv 1c-enterprise-8.3.19.1150-ws_8.3.19-1150_amd64.deb 1c/1c-ws/8.3.19-1150/
```
2. Собираем образ:
```
docker build -t 1c-ws:8.3.19-1150 ./1c/1c-ws/
```
3. Запускаем веб-сервер:
```
docker run -d \
           --add-host="1c-server.example.com:192.168.0.1" \
        1c-ws:8.3.19-1150
```

- Пример файла _default.vrd_ для настройки входа по протоколу OpenID Connect с помощью Keycloak:
```
<?xml version="1.0" encoding="UTF-8"?>
<point xmlns="http://v8.1c.ru/8.2/virtual-resource-system"
        xmlns:xs="http://www.w3.org/2001/XMLSchema"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        base="/base"
        ib="Srvr=1c-server;Ref=base;">
    <openidconnect>
        <providers><![CDATA[[
        {
            "name": "Keycloak",
            "title": "Keycloak",
            "discovery": "https://key.example.com/auth/realms/master/.well-known/openid-configuration",
            "authenticationClaimName": "preferred_username",
            "clientconfig": {
                "authority": "https://key.example.com/auth/realms/master/",
                "client_id": "1c",
                "client_secret": "somesupersecret",
                "redirect_uri": "https://1c.example.com/base/authform.html",
                "response_type": "id_token token",
                "scope": "openid email" 
            }
        }
        ]]]>
        </providers>
    </openidconnect>
	<standardOdata enable="false"
            reuseSessions="autouse"
			sessionMaxAge="20"
			poolSize="10"
			poolTimeout="5"/>
</point>
```


## Клиент

1. Извлекаем файлы для установки клиента:
```
tar -xzf deb64_8_3_19_1150.tar.gz 1c-enterprise-8.3.19.1150-common_8.3.19-1150_amd64.deb
tar -xzf deb64_8_3_19_1150.tar.gz 1c-enterprise-8.3.19.1150-server_8.3.19-1150_amd64.deb
tar -xzf client_8_3_19_1150.deb64.tar.gz 1c-enterprise-8.3.19.1150-client_8.3.19-1150_amd64.deb
mv 1c-enterprise-8.3.19.1150-common_8.3.19-1150_amd64.deb 1c/1c-client/8.3.19-1150/
mv 1c-enterprise-8.3.19.1150-server_8.3.19-1150_amd64.deb 1c/1c-client/8.3.19-1150/
mv 1c-enterprise-8.3.19.1150-client_8.3.19-1150_amd64.deb 1c/1c-client/8.3.19-1150/
```
2. Собираем образ:
```
docker build -t 1c-client:8.3.19-1150 ./1c/1c-client/
```
3. Запускаем клиент:
```
docker run -d \
           -p 5901:5901 \
           -e VNC_PASSWORD=password \
           -e VNC_RESOLUTION=800x600 \
           --add-host="1c-server.example.com:192.168.0.1" \
        1c-client:8.3.19-1150
```
