#!/usr/bin/env bash

# Запускаем менеджер лицензий HASP
/etc/init.d/haspd start

# Запускаем сервер администрирования 1С
gosu usr1cv8 /opt/1cv8/x86_64/ras cluster --daemon

# Список сеансов
# /opt/1cv8/x86_64/rac session --cluster=$(/opt/1cv8/x86_64/rac cluster list | grep cluster | awk '{print $3}') list | grep '^session\|user-name\|host\|app-id\|last-active-at\|last-active-at'

chown -R usr1cv8:grp1cv8 /home/usr1cv8

if [ "$1" = "ragent" ]
then
    exec gosu usr1cv8 /opt/1cv8/x86_64/ragent
fi


exec "$@"
