#!/usr/bin/env bash

rm -rf "$HOME/.vnc"
mkdir -p "$HOME/.vnc"

echo '#!/bin/sh
[ -x /etc/vnc/xstartup ] && exec /etc/vnc/xstartup
[ -r $HOME/.Xresources ] && xrdb $HOME/.Xresources
xsetroot -solid grey
vncconfig -iconic -nowin &
startxfce4 &' > "$HOME/.vnc/xstartup"

chmod +x "$HOME/.vnc/xstartup"

printf "$VNC_PASSWORD\n$VNC_PASSWORD\n\n" | vncpasswd

vncserver -kill :1 || rm -rfv /tmp/.X*-lock /tmp/.X11-unix
vncserver -geometry $VNC_RESOLUTION :1
tail -f $HOME/.vnc/*1.log


exec "$@"
